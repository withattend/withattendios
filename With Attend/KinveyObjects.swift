//
//  KinveyObjects.swift
//  WithAttend
//
//  Created by Brandon Mayhew on 2016-03-28.
//  Copyright © 2016 MayU Studios. All rights reserved.
//

import Foundation

class UpdateBeaconZone: NSObject {    //all NSObjects in Kinvey implicitly implement KCSPersistable
    var entityId: String? //Kinvey entity _id
    var date: String?
    var username : String?
    var location: String?
    var onsite: NSNumber?
    var uuid: String?
    var major: NSNumber?
    var minor: NSNumber?
    var proximity: NSNumber?
    var rssi: NSNumber?
    var accuracy: NSNumber?
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "date" : "date",
            "username" : "username" ,
            "location" : "location",
            "onsite" : "onsite",
            "uuid" : "uuid",
            "major" : "major",
            "minor" : "minor",
            "proximity" : "proximity",
            "rssi" : "rssi",
            "accuracy" : "accuracy",
        ]
    }
}

class UserStatusUpdateArriving: NSObject{
    var entityId: String? //Kinvey entity _id
    var date: String? //what was the date?
    var onsite: NSNumber? //are they on the site?
    var username : String?
    var location : String?
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "date" : "date",
            "onsite" : "onsite",
            "username" : "username" ,
            "location" : "location",
        ]
    }
    
}

class UserStatusUpdateLeaving: NSObject{
    var entityId: String? //Kinvey entity _id
    var date: String? //what was the date?
    var name: String? //what is this event
    var onsite: NSNumber? //are they on the site?
    var username : String?
    var location : String?
    
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId,
            "date" : "date",
            "name" : "name",
            "onsite" : "onsite",
            "username" : "username" ,
            "location" : "location",
        ]
    }

}