//
//  ViewController.swift
//  With Attend
//
//  Created by Brandon Mayhew on 2016-03-24.
//  Copyright © 2016 MayU Studios. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    let locationManager = CLLocationManager()
    //Store Array of Images
    var imageArray = ["riverparkplace","uptown","mayfair","jamesonhouse"]
    //Array of Image Names
    var textArray = ["River Park Place","Uptown","MayFair", "Jameson House",]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.requestAlwaysAuthorization()
        if KCSUser.activeUser() == nil {
            print("User Not Logged In")
            performSegueWithIdentifier("jobsiteTOLogin", sender: nil)
            //presentViewController(loginViewController, animated: true, completion: nil)
            } else {
            //user is logged in and will be loaded on first call to Kinvey
            var currentusername = KCSUser.activeUser().givenName
            print("User named:\(currentusername) has logged in ")
        }
        
        self.navigationController?.navigationBarHidden = false
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell!
        
        //cell.textLabel?.text = textArray[indexPath.row]
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = UIImage(named: imageArray[indexPath.row])
        
        let textLabel2 = cell.viewWithTag(2) as! UILabel
        textLabel2.text = textArray[indexPath.row]
        
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    //On Click
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            //Segue to view
            self.performSegueWithIdentifier("jobsitetoattend", sender: self)
            print("Wanting to segue way -----> Going to prepareforsegue")
        }else if (indexPath.row == 1){
            print("Attempting to segue to Coquitlam Jobsite")
            self.performSegueWithIdentifier("jobsitetoattendCoquitlam", sender: self)
        }else{
            //seguefailed
            print("Segue Failed")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "jobsitetoattend")      {
            segue.destinationViewController as? attendViewController
        }
    }
    
    //Buttons
    @IBAction func logoutButton(sender: AnyObject) {
        if KCSUser.activeUser() == nil{
            //User is not logged in
            print("Cant log out user since they are not logged in!!")
            
        }else{
            KCSUser.activeUser().logout()
            print("User Logged out")
            performSegueWithIdentifier("jobsiteTOLogin", sender: nil)
            
        }
    }
    
    
    
}

