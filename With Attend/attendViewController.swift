//
//  attendViewController.swift
//  With Attend
//
//  Created by Joshua Unrau on 2016-03-24.
//  Copyright © 2016 MayU Studios. All rights reserved.
//
import UIKit
import CoreBluetooth

struct attendViewControllerConstant {
    static let storedItemsKey = "storedItems"
}


class attendViewController: UIViewController, CLLocationManagerDelegate, CBPeripheralManagerDelegate{
    var items: [Item] = []
    let locationManager = CLLocationManager()
    var bluetoothPeripheralManager: CBPeripheralManager?
    var onSiteBool = false
    var atWorkBool = false
    var lastDiscoveredBeacon: NSNumber = -1
    var beaconUUID = "F7826DA6-4FA2-4E98-8024-BC5B71E0893E"
    let beaconRegion =  CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, identifier: "Kontakt")
    let beacon = Item(name: "Kontakt", uuid: NSUUID(UUIDString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, majorValue: 20600)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let options = [CBCentralManagerOptionShowPowerAlertKey:0]
        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: options)
        if KCSUser.activeUser() == nil {
            print("User Not Logged In")
            performSegueWithIdentifier("seguelogin", sender: nil)
        }
        if  !NSUserDefaults.standardUserDefaults().boolForKey("firstAppLoad"){
            //it is not the first time the app has been loaded
            NSLog("Not first app load")
        }
        else
        {
            NSLog("First app load")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "firstAppLoad");
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "atWork");
        }
        locationManager.delegate = self
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse)
        {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeaconsInRegion(beaconRegion)
        startMonitoringItem(beacon)
        updateUI()
        //loadItems()
        NSLog("loading Items to be monitored")
        startMonitoringItem(beacon)
        //persistItems()
        // Do any additional setup after loading the view.
    }

    func viewDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        updateUI()
        let beacon = Item(name: "Kontakt", uuid: NSUUID(UUIDString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, majorValue: 20600)
        //Original code
        //F7826DA6-4FA2-4E98-8024-BC5B71E0893E
        //2247
        //23294
        //loadItems()
        locationManager.delegate = self
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse)
        {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeaconsInRegion(beaconRegion)
        startMonitoringItem(beacon)
    }

    //ELEMENTS----------------------------
    @IBOutlet weak var adviceText: UILabel!
    @IBOutlet var userStatusText: UILabel!
    @IBOutlet var currentBeaconStatus: UILabel!
    @IBOutlet weak var userStatusButtonOutlet: UIButton!
    @IBOutlet weak var questionText: UILabel!
    @IBAction func userStatusButton(sender: AnyObject)
    {
        NSLog("Pressing button")
        let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        if(NSUserDefaults.standardUserDefaults().boolForKey("buttonType"))// yes button
        {
            var progressView : UIProgressView?
            let alertView = UIAlertController(title: "Please wait", message: "Uploading your log to Attend.", preferredStyle: .Alert)
            presentViewController(alertView, animated: true, completion: {
                //  Add your progressbar after alert is shown (and measured)
                let margin:CGFloat = 8.0
                let rect = CGRectMake(margin, 72.0, alertView.view.frame.width - margin * 2.0 , 2.0)
                progressView = UIProgressView(frame: rect)
                progressView?.progress = 0.1
                progressView?.tintColor = UIColor.blueColor()
                alertView.view.addSubview(progressView!)
            })
            if KCSUser.activeUser() == nil
            {
                //show log-in views
                NSLog("User is not active");
            }
            else
            {
                progressView?.progress = 0.5
                //user is logged in and will be loaded on first call to Kinvey
                let collection = KCSCollection(fromString: "UserStatusArriving", ofClass: UserStatusUpdateLeaving.self)
                let store = KCSAppdataStore(collection: collection, options: nil)
                let event = UserStatusUpdateLeaving()
                if(lastDiscoveredBeacon != -1){
                    event.location = String(lastDiscoveredBeacon)
                }
                else{
                    event.location = "0"
                }
                event.name = "User status updaye"
                event.username = KCSUser.activeUser().username
                event.date = timestamp
                event.onsite = true
                store.saveObject(event, withCompletionBlock:
                {
                        (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                        if errorOrNil != nil
                        {
                            //save failed
                            NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                        }
                        else
                        {
                            //save was successful
                            progressView?.progress = 1
                            NSLog("Successfully saved event in foreground(id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "atWork")
                            self.updateUI()
                            alertView.dismissViewControllerAnimated(true, completion: nil)
                            let message = ""+String(timestamp)
                            let alert = UIAlertView(
                                title: NSLocalizedString("You have been signed into work", comment: ""),
                                message: message,
                                delegate: nil,
                                cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
                                
                            )
                            print("User says they are at work")
                            alert.show()
                    }
                },withProgressBlock: nil
                )
            }
        }
        if(!NSUserDefaults.standardUserDefaults().boolForKey("buttonType"))// no/lunch button
        {
            //user says they are not at work
            NSLog("User says they are not at work")
            var progressView : UIProgressView?
            let alertView = UIAlertController(title: "Please wait", message: "Uploading your log to Attend.", preferredStyle: .Alert)
            //  Show it to your users
            presentViewController(alertView, animated: true, completion: {
                //  Add your progressbar after alert is shown (and measured)
                let margin:CGFloat = 8.0
                let rect = CGRectMake(margin, 72.0, alertView.view.frame.width - margin * 2.0 , 2.0)
                progressView = UIProgressView(frame: rect)
                progressView?.progress = 0.1
                progressView?.tintColor = UIColor.blueColor()
                alertView.view.addSubview(progressView!)
            })
            
            //  Show it to your users
            if KCSUser.activeUser() == nil
            {
                //show log-in views
                NSLog("User is not active");
            }
            else
            {
                //user is logged in and will be loaded on first call to Kinvey
                progressView?.progress = 0.5
                let collection = KCSCollection(fromString: "UserStatusLeaving", ofClass: UserStatusUpdateLeaving.self)
                let store = KCSAppdataStore(collection: collection, options: nil)
                let event = UserStatusUpdateLeaving()
                if(lastDiscoveredBeacon != -1){
                    event.location = String(lastDiscoveredBeacon)
                }
                else{
                    event.location = "0"
                }
                event.name = "User status updaye"
                event.username = KCSUser.activeUser().username
                event.date = timestamp
                event.onsite = false
    
                store.saveObject(event, withCompletionBlock:
                {
                    (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                        if errorOrNil != nil
                        {
                            //save failed
                            NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                        }
                        else
                        {
                            //save was successful
                            progressView?.progress = 1
                            NSLog("Successfully saved event in foreground(id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "atWork")
                            self.updateUI()
                            alertView.dismissViewControllerAnimated(true, completion: nil)
                            let message = ""+String(timestamp)
                            let alert = UIAlertView(
                                title: NSLocalizedString("You have been signed out of work", comment: ""),
                                message: message,
                                delegate: nil,
                                cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
                            )
                            alert.show()
                            
                        }
                },withProgressBlock: nil)
            }
        }
    }
    
    //==================================== Functions
    func uploadUserStatus( _location: String, _name: String, _date: String, _username: String, _onsite: Bool, _dataObject: KCSCollection ){
        //user is logged in and will be loaded on first call to Kinvey
        let collection = _dataObject
        let store = KCSAppdataStore(collection: collection, options: nil)
        let event = UserStatusUpdateArriving()
        if(lastDiscoveredBeacon != -1){
            event.location = String(lastDiscoveredBeacon)
        }
        else{
            event.location = "0"
        }
        event.username = _username
        event.date = _date
        event.onsite = _onsite
        store.saveObject(event, withCompletionBlock:
            {
                (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil
                {
                    //save failed
                    NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                }
                else
                {
                    //save was successful
                    NSLog("Successfully saved event in foreground(id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                    self.updateUI()
                }
            },withProgressBlock: nil
        )
    }
    
    func updateUI(){
         if(NSUserDefaults.standardUserDefaults().boolForKey("onsite") && NSUserDefaults.standardUserDefaults().boolForKey("bluetooth")){
            //are they in the beacon zone
            currentBeaconStatus.text = "You are in the beacon zone"
            NSLog("User is in the beacon zone")
            if(NSUserDefaults.standardUserDefaults().boolForKey("atWork")){
                //at work
                NSLog("User is at work")
                if let image = UIImage(named: "xmark.png") {
                    questionText.text = "Are you leaving work?"
                    userStatusButtonOutlet.setImage(image, forState: .Normal)
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: "buttonType")
                }
                userStatusButtonOutlet.enabled = true
                self.userStatusText.text = "You are checked in for work"
                self.adviceText.hidden = true;
            }
            else{
                //not at work
                questionText.text = "Are you at work?"
                NSLog("User is not at work")
                if let image = UIImage(named: "checkmark.png") {
                    userStatusButtonOutlet.setImage(image, forState: .Normal)
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "buttonType")
                }
                userStatusButtonOutlet.enabled = true
                self.userStatusText.text = "You are not checked in for work"
                self.adviceText.hidden = false;
            }
        }
        else{
            //they are not in the beacon zone
            currentBeaconStatus.text = "You are not in the beacon zone"
            NSLog("User is not in the beacon zone")
            if(NSUserDefaults.standardUserDefaults().boolForKey("atWork") ){
                //at work
                NSLog("User is at work")
                userStatusButtonOutlet.enabled = false
                self.userStatusText.text = "You are checked in for work"
                self.adviceText.hidden = false;
                if let image = UIImage(named: "xmark.png") {
                    questionText.text = "Are you leaving work?"
                    userStatusButtonOutlet.setImage(image, forState: .Normal)
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: "buttonType")
                }
            }
            else{
                
                questionText.text = "Are you at work?"
                NSLog("User is not at work")
                //not at work
                if let image = UIImage(named: "greymark.png") {
                    userStatusButtonOutlet.setImage(image, forState: .Normal)
                }
                self.userStatusText.text = "You are not checked in for work"
                self.adviceText.hidden = false;
                userStatusButtonOutlet.enabled = false
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "buttonType")
            }
        }
    }
    
    func loadItems() {
        if let storedItems = NSUserDefaults.standardUserDefaults().arrayForKey(attendViewControllerConstant.storedItemsKey) {
            for itemData in storedItems {
                let item = NSKeyedUnarchiver.unarchiveObjectWithData(itemData as! NSData) as! Item
                items.append(item)
                startMonitoringItem(item)
            }
        }
    }
    func beaconRegionWithItem(item:Item) -> CLBeaconRegion {
        let beaconRegion = CLBeaconRegion(proximityUUID: item.uuid, major: item.majorValue, identifier: item.name)
        return beaconRegion
    }
    
    func startMonitoringItem(item:Item) {
        NSLog("Monitoring for beacon")
        let beaconRegion = beaconRegionWithItem(item)
        locationManager.startMonitoringForRegion(beaconRegion)
        locationManager.startRangingBeaconsInRegion(beaconRegion)
    }
    
    func stopMonitoringItem(item:Item) {
        let beaconRegion = beaconRegionWithItem(item)
        locationManager.stopMonitoringForRegion(beaconRegion)
        locationManager.stopRangingBeaconsInRegion(beaconRegion)
    }
    
    func persistItems() {
        var itemsDataArray:[NSData] = []
        for item in items {
            let itemData = NSKeyedArchiver.archivedDataWithRootObject(item)
            itemsDataArray.append(itemData)
        }
        NSUserDefaults.standardUserDefaults().setObject(itemsDataArray, forKey: attendViewControllerConstant.storedItemsKey)
    }
    
    //Elements
        //Label
    @IBOutlet var jobsiteNameLabel: UILabel!
        //Jobsite Image
    @IBOutlet var jobsiteImage: UIImageView!
    
    var jobsiteName = String()
    
    override func viewWillAppear(animated: Bool) {
        jobsiteNameLabel.text = jobsiteName
        
    }
   
    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager!) {
        
        var statusMessage = ""
        
        switch peripheral.state {
        case CBPeripheralManagerState.PoweredOn:
            statusMessage = "Bluetooth Status: Turned On"
            if(NSUserDefaults.standardUserDefaults().boolForKey("bluetooth") == false){
                showThankYouBluetoothAlert()
            }
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "bluetooth");
            self.updateUI()
        case CBPeripheralManagerState.PoweredOff:
            statusMessage = "Bluetooth Status: Turned Off"
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "bluetooth");
            self.updateUI()
            showBluetoothAlert()
        case CBPeripheralManagerState.Resetting:
            statusMessage = "Bluetooth Status: Resetting"
            
        case CBPeripheralManagerState.Unauthorized:
            statusMessage = "Bluetooth Status: Not Authorized"
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "bluetooth");
            self.updateUI()
            showBluetoothAlert()
        case CBPeripheralManagerState.Unsupported:
            statusMessage = "Bluetooth Status: Not Supported"
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "bluetooth");
            self.updateUI()
            showBluetoothAlert()
        default:
            statusMessage = "Bluetooth Status: Unknown"
        }
        
        NSLog(statusMessage)
        
        if peripheral.state == CBPeripheralManagerState.PoweredOff {
            //TODO: Update this property in an App Manager class
        }
    }
}
func showBluetoothAlert(){
    let message = "Attend needs bluetooth to be on to work. Please turn on bluetooth in settings"
    let alert = UIAlertView(
        title: NSLocalizedString("Your Bluetooth is not on.", comment: ""),
        message: message,
        delegate: nil,
        cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
    )
    alert.show()
}
func showThankYouBluetoothAlert(){
    let message = ""
    let alert = UIAlertView(
        title: NSLocalizedString("Thank you for turning on bluetooth.", comment: ""),
        message: message,
        delegate: nil,
        cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
    )
    alert.show()
}
extension attendViewController
{
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons:[CLBeacon], inRegion region: CLBeaconRegion)
    {
        NSLog("Ranging beacons")
        if (beacons.count >= 1)
        {
            var i = 0
            NSLog(String(beacons.count))
            while(i < beacons.count)
            {
                NSLog(String(beacons[i].minor))
                lastDiscoveredBeacon = beacons[i].minor;
                i += 1
                break;
            }
            NSLog("Found a beacon")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "onsite");
            //there is at least one close beacon
        }
        else
        {
            NSLog("Didnt find a beacon")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "onsite");
        }
        self.updateUI()
    }
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion)
    {
        let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        if let beaconRegion = region as? CLBeaconRegion
        {
            let notification = UILocalNotification()
            notification.alertBody = "You are not in the beacon zone"
            notification.soundName = "Default"
            if KCSUser.activeUser() == nil
            {
                //show log-in views
                NSLog("User is not active");
            }
            else
            {
                //user is logged in and will be loaded on first call to Kinvey
                let collection = KCSCollection(fromString: "Events", ofClass: UpdateBeaconZone.self)
                let store = KCSAppdataStore(collection: collection, options: nil)
                let event = UpdateBeaconZone()
                event.location = "RiverParkPlace" //not actual beacon name (yet)
                event.username = KCSUser.activeUser().username;
                event.date = timestamp //sample date
                event.onsite = false;
                store.saveObject(event, withCompletionBlock:
                    {
                        (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                        if errorOrNil != nil
                        {
                            //save failed
                            NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                        }
                        else
                        {
                            //save was successful
                            NSLog("Successfully saved event in background (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "onsite");
                            self.updateUI()
                        }
                    },withProgressBlock: nil
                )
            }
            
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion)
    {
        let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        if let beaconRegion = region as? CLBeaconRegion
        {
            /*var i = 0
             func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!){
             let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
             while i < knownBeacons.count{
             var uuid = knownBeacons[i].proximityUUID
             var major = knownBeacons[i].major
             var minor = knownBeacons[i].minor
             var proximity = knownBeacons[i].proximity.rawValue
             var rssi = knownBeacons[i].rssi
             var accuracy = knownBeacons[i].accuracy.description
             print(uuid)
             }
             }*/
            
            let notification = UILocalNotification()
            notification.alertBody = "You are in the beacon zone"
            notification.soundName = "Default"
            
            if KCSUser.activeUser() == nil
            {
                //show log-in views
                NSLog("User is not active");
            }
            else
            {
                //user is logged in and will be loaded on first call to Kinvey
                let collection = KCSCollection(fromString: "Events", ofClass: UpdateBeaconZone.self)
                let store = KCSAppdataStore(collection: collection, options: nil)
                let event = UpdateBeaconZone()
                event.location = "RiverParkPlace" //not actual beacon name (yet)
                event.username = KCSUser.activeUser().username
                event.date = timestamp
                event.onsite = true
                store.saveObject(event, withCompletionBlock:
                {
                    (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                    if errorOrNil != nil
                    {
                        //save failed
                        NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                    }
                    else
                    {
                        //save was successful
                        NSLog("Successfully saved event in foreground(id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "onsite");
                        self.updateUI()
                    }
                },withProgressBlock: nil
            )
            }
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }
        
}