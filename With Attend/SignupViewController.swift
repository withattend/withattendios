//
//  SignupViewController.swift
//  WithAttend
//
//  Created by Brandon Mayhew on 2016-03-28.
//  Copyright © 2016 MayU Studios. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Func
    func signup(){
        let username = usernameTF.text
        let firstname = firstnameTF.text
        let lastname = lastnameTF.text
        let email = emailTF.text
        //var company = "nova"
        let password = passwordTF.text
        
        if(username == "" || firstname == "" || lastname == "" || email == "" || password == ""){
            //You are missing data
            print("You are missing data in the textfields")
            //   popup() ---> You cant show a pop up beacuse of the hidden view controller
        }else{
            //All textfiels have information
            print("All textfields have data")
            print("Attempting  to create user")
            KCSUser.userWithUsername(
                username,
                password: password,
                fieldsAndValues:[
                    KCSUserAttributeEmail : email!,
                    KCSUserAttributeGivenname : firstname!,
                    KCSUserAttributeSurname : lastname!
                ]
                ,
                withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                    if errorOrNil == nil {
                        //user is created
                        print("User created")
                        self.performSegueWithIdentifier("signuptologin", sender: nil)
                    } else {
                        //there was an error with the create
                        print("----User error-----")
                        print("\(username)")
                        print("\(email)")
                        print("\(password)")
                        print("\(firstname)")
                        print("\(lastname)")
                        print("---End of Error---")
                        
                        
                    }
                }
            )

            
        }
        
    
    
            }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        usernameTF.resignFirstResponder()
        firstnameTF.resignFirstResponder()
        lastnameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
    }
    
    
    //Textfields
        //signup
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var firstnameTF: UITextField!
    @IBOutlet weak var lastnameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    //Buttons
    @IBAction func signup(sender: AnyObject) {
        signup()
    }
    
    //functions
    
    func popup(){
        let message = "You Are Missing Information!"
        let alert = UIAlertView(
            title: NSLocalizedString("Please Fill Out All The Fields!", comment: ""),
            message: message,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("I will Add Information", comment: "I will Add Information"))
        
    }
    
}
