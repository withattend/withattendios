import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let notificationType:UIUserNotificationType = [UIUserNotificationType.Sound, UIUserNotificationType.Alert]
        let notificationSettings = UIUserNotificationSettings(forTypes: notificationType, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
        KCSClient.sharedClient().initializeKinveyServiceForAppKey(
            "kid_b1ttsn1CkW",
            withAppSecret: "302e2692c1434e818a64ad34c049041a",
            usingOptions: nil
        )
        locationManager.delegate = self
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// MARK: CLLocationManagerDelegate
extension AppDelegate: CLLocationManagerDelegate
{
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion)
    {
        if let beaconRegion = region as? CLBeaconRegion
        {
            let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
            let notification = UILocalNotification()
            notification.alertBody = "You are not in the beacon zone"
            notification.soundName = "Default"
            if KCSUser.activeUser() == nil
            {
                //show log-in views
                NSLog("User is not active");
            }
            else
            {
                //user is logged in and will be loaded on first call to Kinvey
                let collection = KCSCollection(fromString: "Events", ofClass: UpdateBeaconZone.self)
                let store = KCSAppdataStore(collection: collection, options: nil)
                let event = UpdateBeaconZone()
                event.location = String(beaconRegion.minor) //not actual beacon name (yet)
                event.username = KCSUser.activeUser().username
                event.date = timestamp //sample date
                event.onsite = false;
                store.saveObject(event, withCompletionBlock:
                    {
                        (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                        if errorOrNil != nil
                        {
                            //save failed
                            NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                        }
                        else
                        {
                            //save was successful
                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "onsite");
                            NSLog("Successfully saved event in background (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                        }
                    },withProgressBlock: nil
                )
            }
            
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion)
    {
        let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        if let beaconRegion = region as? CLBeaconRegion
        {
            /*var i = 0
             func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!){
             let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
             while i < knownBeacons.count{
             var uuid = knownBeacons[i].proximityUUID
             var major = knownBeacons[i].major
             var minor = knownBeacons[i].minor
             var proximity = knownBeacons[i].proximity.rawValue
             var rssi = knownBeacons[i].rssi
             var accuracy = knownBeacons[i].accuracy.description
             print(uuid)
             }
             }*/
            
            let notification = UILocalNotification()
            notification.alertBody = "You are in the beacon zone"
            notification.soundName = "Default"
            
            if KCSUser.activeUser() == nil
            {
                //show log-in views
                NSLog("User is not active");
            }
            else
            {
                //user is logged in and will be loaded on first call to Kinvey
                let collection = KCSCollection(fromString: "Events", ofClass: UpdateBeaconZone.self)
                let store = KCSAppdataStore(collection: collection, options: nil)
                let event = UpdateBeaconZone()
                event.location = String(beaconRegion.minor)
                event.username = KCSUser.activeUser().username
                event.date = timestamp
                event.onsite = true;
                store.saveObject(event, withCompletionBlock:
                    {
                        (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                        if errorOrNil != nil
                        {
                            //save failed
                            NSLog("Save failed, with error: %@", errorOrNil.localizedFailureReason!)
                        }
                        else
                        {
                            //save was successful
                            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "onsite");
                            NSLog("Successfully saved event in background(id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                        }
                    },withProgressBlock: nil
                )
            }
            
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }
}

