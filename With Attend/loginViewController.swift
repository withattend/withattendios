//
//  loginViewController.swift
//  WithAttend
//
//  Created by Brandon Mayhew on 2016-03-28.
//  Copyright © 2016 MayU Studios. All rights reserved.
//

import UIKit

class loginViewController: UIViewController {
    var username: String = ""
    var password: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        if(NSUserDefaults.standardUserDefaults().boolForKey("rememberMe")){//remember me
            username = NSUserDefaults.standardUserDefaults().stringForKey("username")!
            password =  NSUserDefaults.standardUserDefaults().stringForKey("password")!
            usernameLOGINTF.text = username
            passwordLOGINTF.text = password
            rememberMeSwitch.on = true;
        }
        else{
            rememberMeSwitch.on = false;
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Functions

    func login(){
        username = usernameLOGINTF.text! //Get username from textfield
        password = passwordLOGINTF.text! //Get password from textfield
        
        //Ask Kinvey to log user in (Below)
        if KCSUser.activeUser() == nil{
            //User is not logged in
            print("Cant log out user since they are not logged in!!")
            
        }else{
            KCSUser.activeUser().logout()
            print("User Logged out")
        }
        KCSUser.loginWithUsername(username, password: password, withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
            if errorOrNil == nil {
                //the log-in was successful and the user is now the active user and credentials saved
                //hide log-in view and show main app content
                print("Log in Success")
                if(NSUserDefaults.standardUserDefaults().boolForKey("rememberMe")){
                    NSUserDefaults.standardUserDefaults().setValue(self.username, forKey: "username");
                    NSUserDefaults.standardUserDefaults().setValue(self.password, forKey: "password");
                }
                self.performSegueWithIdentifier("seguetoscan", sender: nil)
            } else {
                //there was an error with the update save
                let message = errorOrNil.localizedDescription
                let alert = UIAlertView(
                    title: NSLocalizedString("Login Failed", comment: "Hint: Capitals Do matter"),
                    message: message,
                    delegate: nil,
                    cancelButtonTitle: NSLocalizedString("OK", comment: "OK")
                )
                print("Sign Up Failed")
                alert.show()
            }
            }
        )
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) { //Checks to see if the user clicked somewhere offscreen
        usernameLOGINTF.resignFirstResponder() //Resign username keyboard that appears
        passwordLOGINTF.resignFirstResponder() //Resign password keyboard that appears
    }

    //TextFields
        //Login
    @IBAction func rememberMeToggle(sender: AnyObject) {
        if((sender.on) != nil){
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "rememberMe");
        }
        else{
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "rememberMe");
        }
    }
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    @IBOutlet weak var usernameLOGINTF: UITextField! //Username Textfield
    @IBOutlet weak var passwordLOGINTF: UITextField! //Password Textfield
    
    //Buttons
    @IBAction func login(sender: AnyObject) {//User clicks login button
        login() //Login function called. Execute login function
    }
    
}
